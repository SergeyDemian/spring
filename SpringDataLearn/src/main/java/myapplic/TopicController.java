package main.java.myapplic;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TopicController {

	
	@Autowired
	TopicsService topicsService;
	
	@RequestMapping(
			method=RequestMethod.GET,
			value="/topics"
			)
	public List<Topic> getAllTopic() {
		return topicsService.getAllTopic();
	}
	
	@RequestMapping(
			method=RequestMethod.POST,
			value="/topics"
			)
	public void addTopic (@RequestBody Topic topic) {
		topicsService.addTopic(topic);
	}
	
	@RequestMapping(
			method=RequestMethod.GET,
			value="/topics/{id}"
			)
	public Optional<Topic> getTopic (@PathVariable String id) {
		return topicsService.getTopic(id);
	}
	
	@RequestMapping(
			method=RequestMethod.DELETE,
			value="/topics/{id}"
			)
	public void deleteTopic(@PathVariable String id) {
		topicsService.deleteTopic(id);
	}
	
	@RequestMapping(
			method=RequestMethod.PUT,
			value="/topics/{id}"
			)
	public void update (@PathVariable String id, @RequestBody Topic topic) {
		topicsService.update(id, topic);
	}
}
