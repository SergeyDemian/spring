package main.java.myapplic;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TopicsService {

	@Autowired
	TopicRepository topicRepository;
	
	public List<Topic> getAllTopic (){
		List<Topic> topics = new ArrayList<>();
		topicRepository.findAll()
		.forEach(topics::add);
		return topics;
	}
	
	public void addTopic (Topic topic) {
		topicRepository.save(topic);
	}
	
	public Optional<Topic> getTopic (String id) {
		return topicRepository.findById(id);
	}
	
	public void deleteTopic (String id) {
		topicRepository.deleteById(id);
	}

	public void update(String id, Topic topic) {
		topicRepository.save(topic);
		
	}
}
