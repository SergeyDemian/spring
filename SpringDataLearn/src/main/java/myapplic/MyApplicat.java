package main.java.myapplic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyApplicat {

	public static void main(String[] args) {
		
		SpringApplication.run(MyApplicat.class, args);

	}

}
