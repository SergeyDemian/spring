
import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@EnableAutoConfiguration
public class MyClass {

	@RequestMapping("/")
	String home () {
		return "Hello World";
	}
	
	public static void main(String[] args) throws Exception {
		SpringApplication app = new SpringApplication(MyClass.class);
		app.setBannerMode(Banner.Mode.LOG);
		app.setAddCommandLineProperties(false);
		app.run(args);

	}

}
