package main.java;

import java.util.Arrays;
import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TicketTopics {

	@RequestMapping(
			value = "/ticTop"
			)
	public List<Topic> getTopics () {
		return Arrays.asList(
				new Topic("1", "ticket1", "2018"),
				new Topic("2", "ticket2", "2019"),
				new Topic("3", "ticket3", "2020")
				);
				
	}
}
