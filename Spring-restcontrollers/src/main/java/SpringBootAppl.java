package main.java;

import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootAppl {

	public static void main(String[] args) {
		
		SpringApplication appl = new SpringApplication(SpringBootAppl.class);
		appl.setBannerMode(Banner.Mode.LOG);
		appl.run(args);
	}

}
