package main.java;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Ticket1 {

	@RequestMapping(
			value = "/tic1"
			)
	public String tic1 () {
		return getClass().getSimpleName();
		
	}
}
