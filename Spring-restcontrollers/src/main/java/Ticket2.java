package main.java;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Ticket2 {

	@RequestMapping(
			value = "/tic2",
			method = RequestMethod.GET
			)
	public String tic2() {
		return getClass().getSimpleName();
	}
}
