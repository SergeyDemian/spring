package main.java;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MyRestController {

	@Autowired
	TopicService topicService;
	
	@RequestMapping(
			value = "/topics"
			)
	public List<Topic> getAllTopics () {
		return topicService.getServiceTopic();
	}
	
	@RequestMapping(
			value = "/topics/{id}"
			)
	public Topic getTopic (@PathVariable String id) {
		return topicService.getTopic(id);
	}
	
	@RequestMapping(
			method=RequestMethod.POST,
			value="/topics"
			)
	public void addTopic(@RequestBody Topic topic) {
		topicService.addTopic(topic);
	}
	
	@RequestMapping(
			method=RequestMethod.PUT,
			value="/topics/{id}"
			)
	public void update(@RequestBody Topic topic, @PathVariable String id) {
		topicService.update(id, topic);
	}
	
	@RequestMapping(
			method=RequestMethod.DELETE,
			value="/topics/{id}"
			)
	public void delete(@PathVariable String id) {
		topicService.delete(id);
	}
}
