package main.java;

import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyApplic {

	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(MyApplic.class);
		app.setBannerMode(Banner.Mode.LOG);
		app.run(args);
		
	}

}
